#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#include "ads1015.h"
#include "log.h"
#include "i2c_func.h"

static int32_t i2c_handle;

int16_t readADC(void) {
  uint8_t buffer[2];
  int16_t reading;

  i2c_set_device_address(i2c_handle, ADC_ADDRESS);
  if (i2c_read_bytes(i2c_handle, ADS1015_CONVERSION_R, 2, buffer) < 2) {
    return -1;
  }

  reading = buffer[0];
  reading <<= 8;
  reading |= buffer[1];

  // Reading is 12 bits 2's complement, left aligned. Make right-aligned and
  // extend sign bit for valid int16_t.
  reading >>= 4;
  if (reading&0x0800) {
    reading |= 0xF000;
  }
  return reading;
}

int8_t setInputADC(uint8_t channel) {
  int8_t rc;
  uint8_t channel_mask = 0;
  uint8_t adc_config[2];

  if (channel > ADS1015_MAX_CHANNEL) {
    return -1;
  }

  i2c_set_device_address(i2c_handle, ADC_ADDRESS);

  channel_mask |= (channel << 4);
  adc_config[0] = ADS1015_CONFIG_MSB|channel_mask;
  adc_config[1] = ADS1015_CONFIG_LSB;
  rc = i2c_write_bytes(i2c_handle, ADS1015_CONFIG_R, 2, adc_config);
  usleep(10000);    // wait 10ms after channel switch
  return rc;
}

int8_t initADC(int32_t i2c_adapter) {
  int8_t rc;
  i2c_handle = i2c_adapter;
  return setInputADC(0);
}
