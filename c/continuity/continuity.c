#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#include "testrig_gpio_defs.h"
#include "mux_channel_defs.h"
#include "test_error_defs.h"
#include "testrig_settings.h"
#include "gpio_pwm.h"
#include "log.h"
#include "i2c_func.h"
#include "continuity.h"
#include "timer_functions.h"
#include "ads1015.h"

/* Default values for MUX to gnd: Set to C15, NC */
static const uint8_t mux_gnd_selects[4] = {MUX_GND_SEL_0, MUX_GND_SEL_1, MUX_GND_SEL_2, MUX_GND_SEL_3};
static const uint8_t mux_gnd_defaults[4] = {1, 1, 1, 1};
/* Default values for MUX to gnd: Set to C0, NC */
static const uint8_t mux_3v3_selects[4] = {MUX_3V3_SEL_0, MUX_3V3_SEL_1, MUX_3V3_SEL_2, MUX_3V3_SEL_3};
static const uint8_t mux_3v3_defaults[4] = {0, 0, 0, 0};

static char* signal_labels[15] = {
  "JTST_GND", "JBAT_GND", "VCHG_GND",
  "VBUS2", "CHIP_SEL", "U2_TX",
  "U2_RX", "JMTR_NEG", "I2C_SCL",
  "I2C_SDA", "V1P8", "JBAT_VBAT",
  "JTST_V3P3", "JMTR_POS", "VCC_OUT"
};

/* utility const: empty column (10 spaces) */
static char* empty_column = "          ";

static int32_t i2c_handle;

static uint8_t mux_gnd_test_order[15] = {
                                  JTST_GND_G, JBAT_GND_G, VCHG_GND_G,
                                  VBUS2_G, CHIP_SELECT_G, U2_TX_G,
                                  U2_RX_G, JMTR_NEG_G, JTST_I2C_SCL_1P8_G,
                                  JTST_I2C_SDA_1P8_G, JTST_V1P8_G, JBAT_VBAT_G,
                                  JTST_V3P3_E_G, JMTR_POS_G, JTST_VCC_OUT_G
                                };

static uint8_t mux_3v3_test_order[15] = {
                                  JTST_VCC_OUT_H, JMTR_POS_H, JTST_V3P3_E_H,
                                  JBAT_VBAT_H, JTST_V1P8_H, JTST_I2C_SDA_1P8_H,
                                  JTST_I2C_SCL_1P8_H, JMTR_NEG_H, U2_RX_H,
                                  U2_TX_H, CHIP_SELECT_H, VBUS2_H,
                                  VCHG_GND_H, JBAT_GND_H, JTST_GND_H
                                };

#define RESULT_FILE_COL_WIDTH (10)

/* Local function prototypes */
static void setMuxSelects(uint8_t channel, const uint8_t *mux_selects);
static uint8_t disableUART1(void);
static void setMuxSelects(uint8_t channel, const uint8_t *mux_selects);
static void set3v3Mux(uint8_t channel);
static void enableMUX(bool enable);

static void setMuxSelects(uint8_t channel, const uint8_t *mux_selects) {
  uint8_t i;
  uint8_t selects[4];

  selects[0] = (channel&0x01)==0 ? 0 : 1;
  selects[1] = (channel&0x02)==0 ? 0 : 1;
  selects[2] = (channel&0x04)==0 ? 0 : 1;
  selects[3] = (channel&0x08)==0 ? 0 : 1;

  for (i=0; i<4; i++) {
    gpio_pin_set_value(mux_selects[i], selects[i]);
  }
}

/*
 *  Function: disableUART1()
 *
 *  Disables UART1 by writing 'mode0' to UART1 pinmux files. Must be run as
 *  root. Deprecated, functionality moved to export_gpio_pins.sh.
 *
 *  Args: None
 *  Returns: 0 on success, 1 on failure.
 */
static uint8_t disableUART1(void) {
  uint8_t i;

  log_info("disable uart on pin 128\n");
  if(system("echo mode0 > /sys/kernel/debug/gpio_debug/gpio128/current_pinmux") == -1) {
    return 1;
  };
  log_info("disable uart on pin 129\n");
  if(system("echo mode0 > /sys/kernel/debug/gpio_debug/gpio129/current_pinmux") == -1) {
    return 1;
  };
  log_info("disable uart on pin 130\n");
  if(system("echo mode0 > /sys/kernel/debug/gpio_debug/gpio130/current_pinmux") == -1) {
    return 1;
  };
  log_info("disable uart on pin 131\n");
  if(system("echo mode0 > /sys/kernel/debug/gpio_debug/gpio131/current_pinmux") == -1) {
    return 1;
  };
  return 0;
}

uint8_t initContinuityCheck(int32_t adapter) {
  uint8_t i, mux_gnd_sel, mux_3v3_sel;
  int8_t rc;


  /* Initialize MUX Selects so that NC pins are selected on both MUXes. */
  for (i=0; i<4; i++) {
    mux_gnd_sel = mux_gnd_selects[i];
    mux_3v3_sel = mux_3v3_selects[i];

    // Configure ground-MUX pin
    gpio_pin_set_direction(mux_gnd_sel, PIN_DIR_OUT);
    gpio_pin_set_value(mux_gnd_sel, mux_gnd_defaults[i]);

    // Configure 3v3-MUX pin
    gpio_pin_set_direction(mux_3v3_sel, PIN_DIR_OUT);
    gpio_pin_set_value(mux_3v3_sel, mux_3v3_defaults[i]);
  }

  if (initADC(adapter) < 0) rc = -1;
  else rc = 0;
  return rc;
}

void print_result_header(FILE *file) {
  uint8_t i;
  char* label;

  fprintf(file, "%s", empty_column);
  for(i=0; i<NUM_CONNECTIONS; i++) {
    // print signals in reverse order for column labels
    fprintf(file, "%10s", signal_labels[NUM_CONNECTIONS-(i+1)]);
  }
  fprintf(file, "\n");
}

/*
 *  toggleMUX
 *  Helper function to enable/disable MUX. 0 enables, 1 disables.
 */
static void enableMUX(bool enable) {
  uint8_t gpio_val;
  gpio_val = enable ? 0 : 1;
  gpio_pin_set_value(EN_MUX, gpio_val);
}

static bool checkResult(gnd_index, hi_index, adc_count) {
  bool result;
  uint8_t gnd_channel, hi_channel;
  gnd_channel = mux_gnd_test_order[gnd_index];
  hi_channel = mux_3v3_test_order[hi_index];
  // Handle special cases: shorts expected
  if ((gnd_channel == JTST_GND_G) && (hi_channel == VCHG_GND_H)) {
    return (adc_count < ADC_SHORT_THRESHOLD);
  }
  if ((gnd_channel == JTST_GND_G) && (hi_channel == JBAT_GND_H)) {
    return (adc_count < ADC_SHORT_THRESHOLD);
  }
  if ((gnd_channel == JBAT_GND_G) && (hi_channel == VCHG_GND_H)) {
    return (adc_count < ADC_SHORT_THRESHOLD);
  }
  if ((gnd_channel == JMTR_POS_G) && (hi_channel == JTST_VCC_OUT_H)) {
    return (adc_count < ADC_SHORT_THRESHOLD);
  }
  return (adc_count > ADC_SHORT_THRESHOLD);
}

static void logContinuityError(uint8_t gnd_index, uint8_t hi_index) {
  uint8_t gnd_channel, hi_channel;
  gnd_channel = mux_gnd_test_order[gnd_index];
  hi_channel = mux_3v3_test_order[hi_index];
  if ((gnd_channel == JTST_GND_G) && (hi_channel == VCHG_GND_H)) {
    log_error("No connection between ground plane and charger ground.");
  } else if ((gnd_channel == JTST_GND_G) && (hi_channel == JBAT_GND_H)) {
    log_error("No connection between ground plane and battery ground.");
  } else if ((gnd_channel == JBAT_GND_G) && (hi_channel == VCHG_GND_H)) {
    log_error("No connection between battery and charger ground.");
  } else if ((gnd_channel == JMTR_POS_G) && (hi_channel == JTST_VCC_OUT_H)) {
    log_error("No connection between motor-positive and VCC.");
  } else {
    log_error("Detected short between %s and %s.", signal_labels[gnd_index], signal_labels[NUM_CONNECTIONS-(hi_index+1)]);
  }
}

/* integer comparison for qsort */
static int int_cmp(const void *a, const void *b) {
  int16_t x = *(const int16_t *)a;
  int16_t y = *(const int16_t *)b;
  return (x < y) ? -1 : (x > y);
}

static int16_t resultFromSamples(int16_t *sampleArr) {
  // sort sampleArr and return middle element to get median
  qsort(sampleArr, 3, sizeof(int16_t), int_cmp);
  return sampleArr[1];
}

uint8_t continuityCheck(int32_t adapter) {
  uint8_t i, j, sample, stop_index;
  int16_t samples[3];
  int16_t row_result[NUM_CONNECTIONS];
  uint64_t start, stop;
  bool pass = true;

  enableMUX(true);
  start = get_time_ms();
  // FILE *result = fopen("continuity.csv", "w");
  FILE *result = stdout;
  print_result_header(result);
  stop_index = NUM_CONNECTIONS - 1;

  for (i=0; i<(NUM_CONNECTIONS-1); i++) {
    setMuxSelects(mux_gnd_test_order[i], mux_gnd_selects);
    for (j=0; j<stop_index; j++) {
      setMuxSelects(mux_3v3_test_order[j], mux_3v3_selects);
      usleep(100000);     // wait 100ms
      for (sample=0; sample<3; sample++) {
        samples[sample] = readADC();
        usleep(10000);  // 10 ms between samples
      }
      row_result[j] = resultFromSamples(samples);
      if (checkResult(i, j, row_result[j]) == false) {
        logContinuityError(i, j);
        pass = false;
      }
    }
    setMuxSelects(MUX_GND_DEFAULT_CHANNEL, mux_gnd_selects);
    setMuxSelects(MUX_3V3_DEFAULT_CHANNEL, mux_3v3_selects);
    for (; j<NUM_CONNECTIONS; j++) row_result[j] = -1;
    fprintf(result, "%10s", signal_labels[i]);
    for (j=0; j<NUM_CONNECTIONS; j++)fprintf(result, "%10d", row_result[j]);
    fprintf(result, "\n");
    stop_index--;
    usleep(100000);
  }
  // fclose(result);
  stop = get_time_ms();
  enableMUX(false);
  log_info("Ran continuity check in %d ms.", (stop-start));
  if (pass) return SUCCESS;
  else return CONTINUITY_CHECK_FAIL;
}
