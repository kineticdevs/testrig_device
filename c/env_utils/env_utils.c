#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <errno.h>
#include <math.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>

#include "globals.h"
#include "ux_manager.h"
#include "log.h"
#include "env_utils.h"

// To enable or disable the run_tests
// Unused in test rig; kept here to compile env parsing w/o modification
uint8_t RUN_TESTS_ENABLE = 0; // 0-> disable and 1->enable

// To enable/disable ZMQ send
// Unused in test rig; kept here to compile env parsing w/o modification
uint8_t ZMQ_SEND = 0; // 0-> disable and 1-> enable

// Skip K points for ZMQ data
// Unused in test rig; kept here to compile env parsing w/o modification
int SKIP_N_POINTS = 1;

// Various environment modes
// NOTE: Defining them 'static' for use in this file only - should be the case
struct env_mode_s env_dev = {.mode_type = ENV_MODE_DEV, .verbosity = 8,
									.heartbeat = 1, .upload_data = 1,
									.ble_enable = 0,
									.wifi_power_management = 0, .vibration_feedback_enable = 1};

struct env_mode_s env_test = {.mode_type = ENV_MODE_TEST, .verbosity = 8,
									 .heartbeat = 1, .upload_data = 1,
									 .ble_enable = 0,
									 .wifi_power_management = 0, .vibration_feedback_enable = 1};

struct env_mode_s env_stage = {.mode_type = ENV_MODE_STAGE, .verbosity = 2,
									  .heartbeat = 0, .upload_data = 1,
									  .ble_enable = 0,
									  .wifi_power_management = 1, .vibration_feedback_enable = 1};

struct env_mode_s env_prod = {.mode_type = ENV_MODE_PROD, .verbosity = 0,
									 .heartbeat = 0, .upload_data = 1,
									 .ble_enable = 0,
									 .wifi_power_management = 1, .vibration_feedback_enable = 1};

struct env_mode_s env_demo = {.mode_type = ENV_MODE_DEMO, .verbosity = 0,
									 .heartbeat = 1, .upload_data = 0,
									 .ble_enable = 0,
									 .wifi_power_management = 0, .vibration_feedback_enable = 1};


// Set up the current environment mode
// It's defined as extern in i2c.h to be used by other files
struct env_mode_s *env_current_mode = &env_dev; // Set to dev mode by default

void hw_specific_vars(unsigned long rev)
{
	switch(rev){
		case 0x070B: // Or 1803 in decimal
			display_hw_config = DISPLAY_ALT_CONFIG;
			display_rotation = DISPLAY_180_DEG;
			break;
		default:
			display_hw_config = DISPLAY_SEQ_CONFIG;
			display_rotation = DISPLAY_0_DEG;
			break;
	}
}

// parse the .env file to get useful environment variables
int8_t parse_dot_env() {
	// char *env_folder = getenv("HOME");
	uint8_t *env_folder = "/home/kinetic";
	char dot_env_location[19];
	strcpy(dot_env_location, env_folder);
	strcat(dot_env_location, "/.env");

	if(file_exists(dot_env_location)) {

		// parse the code for the required variables
		// NOTE: The .env file MUST have ENV_MODE field above all the overwrite fields

		// First open the file and read the environment mode before reading the overwrite variables
		FILE* env_file;
		if((env_file = fopen(dot_env_location, "r")) != NULL) {

			char *var_name;
			char *var_val;
			char line_buffer[1024];
			char *line_buffer_ptr = line_buffer;
			size_t line_max = 1024;

			while(getline(&line_buffer_ptr, &line_max, env_file) != -1) {
				var_name = strtok(line_buffer, "=");
				var_val = strtok(NULL, "");

				if(strcmp(var_name, "ENV_MODE") == 0) {
					switch(strtol(var_val,0,10)){
						case ENV_MODE_DEV:
							env_current_mode = &env_dev;
							break;
						case ENV_MODE_TEST:
							env_current_mode = &env_test;
							break;
						case ENV_MODE_STAGE:
							env_current_mode = &env_stage;
							break;
						case ENV_MODE_PROD:
							env_current_mode = &env_prod;
							break;
						case ENV_MODE_DEMO:
							env_current_mode = &env_demo;
							break;
						default:
							log_warn("Unknown mode type, using default: %d",env_current_mode->mode_type);
					}
				}
			}
		}
		fclose(env_file);

		// parse the code for the required overwrite variables
		// NOTE: The .env file MUST have ENV_MODE field above all the overwrite fields

		// NO OVERWRITE allowed in PRODUCTION MODE
		if(env_current_mode->mode_type != ENV_MODE_PROD){
			FILE* env_file_ovwr;
			if((env_file_ovwr = fopen(dot_env_location, "r")) != NULL) {

				char *var_name;
				char *var_val;
				char line_buffer[1024];
				char *line_buffer_ptr = line_buffer;
				size_t line_max = 1024;

				while(getline(&line_buffer_ptr, &line_max, env_file_ovwr) != -1) {
					var_name = strtok(line_buffer, "=");
					var_val = strtok(NULL, "");

					// Variables to be overwritten
					if(strcmp(var_name, "OVWR_HEARTBEAT") == 0){
						 env_current_mode->heartbeat = strtol(var_val,0,10);
					}else if(strcmp(var_name, "OVWR_VERBOSITY") == 0){
						env_current_mode->verbosity = strtol(var_val,0,10);
					}else if(strcmp(var_name, "OVWR_BLE_ENABLE") == 0){
						env_current_mode->ble_enable = strtol(var_val,0,10);
					}else if(strcmp(var_name, "OVWR_WIFI_POWER_MANAGEMENT") == 0){
						env_current_mode->wifi_power_management = strtol(var_val,0,10);
					}else if(strcmp(var_name, "OVWR_VIBRATION_FEEDBACK_ENABLE") == 0){
						env_current_mode->vibration_feedback_enable = strtol(var_val,0,10);

					// Variable needed to run tests
					} else if(strcmp(var_name, "RUN_TESTS_ENABLE") == 0) {
						RUN_TESTS_ENABLE = strtod(var_val,NULL);
					} else if(strcmp(var_name, "ZMQ_SEND") == 0) {
						ZMQ_SEND = strtod(var_val,NULL);
					} else if(strcmp(var_name, "ZMQ_SEND_SKIP") == 0){
						SKIP_N_POINTS = 5;
					} else if(strcmp(var_name, "HW_REVISION") == 0){
						hw_specific_vars(strtoul(var_val,NULL,0));
					}
				}
			}
			fclose(env_file_ovwr);
		}

	} else {
		log_error(".env file doesn't exist in /home/kinetic folder");
		return -1;
	}

	//if(env_current_mode->verbosity > 0) {
		printf("--- environment variables:\n");
		printf("MODE_TYPE = %d\n",env_current_mode->mode_type);
		printf("VERBOSITY = %d\n", env_current_mode->verbosity);
		printf("BLE_ENABLE = %d\n", env_current_mode->ble_enable);
		printf("WIFI_POWER_MANAGEMENT = %d\n", env_current_mode->wifi_power_management);
		printf("VIBRATION_FEEDBACK_ENABLE = %d\n", env_current_mode->vibration_feedback_enable);
		fflush(stdout);
	//}

	return 1;
}
