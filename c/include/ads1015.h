#ifndef _ADS1015_H_
#define _ADS1015_H_

#include <stdint.h>

#define ADC_ADDRESS           0x48
#define ADS1015_CONVERSION_R  0x00
#define ADS1015_CONFIG_R      0x01
#define ADS1015_MAX_CHANNEL   (3)
#define ADS1015_CONFIG_MSB    0x42        // OS=0, MUX[2:0]=100 (AINp=AIN0, AINn=gnd), PGA[2:0]=001 (FS=4.096V), MODE=0(continuous conversion)
#define ADS1015_CONFIG_LSB    0x83        // DR[2:0]=100 (default datarate), comparator disabled.
#define ADS1015_CHANNEL_MASK  0xCF        // mask for clearing channel bits

int16_t readADC(void);
int8_t setInputADC(uint8_t channel);
int8_t initADC(int32_t i2c_adapter);

#endif
