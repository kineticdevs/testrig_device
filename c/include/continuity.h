#ifndef _CONTINUITY_H_
#define _CONTINUITY_H_

#define MUX_3V3_DEFAULT_CHANNEL   (15)
#define MUX_GND_DEFAULT_CHANNEL   (0)
#define NUM_CONNECTIONS           (15)

uint8_t initContinuityCheck(int32_t adapter);
uint8_t continuityCheck(int32_t adapter);

#endif
