#ifndef _DISPLAY_UTILS_H_
#define _DISPLAY_UTILS_H_

#define MSG_OFFSET_X  20
#define MSG_OFFSET_Y  5
#define STEP_NUM_OFFSET_X 0
#define STEP_NUM_OFFSET_Y 5

void displayCount(uint16_t count);
void bigText(char *text);

#endif
