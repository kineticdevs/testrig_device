#ifndef _ENV_UTILS_H_
#define _ENV_UTILS_H_

#include "globals.h"

/* hacky--just wrap ux_manager.h */
#include "ux_manager.h"

void hw_specific_vars(unsigned long rev);
int8_t parse_dot_env(void);


#endif
