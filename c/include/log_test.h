
#ifndef _LOG_TEST_H_
#define _LOG_TEST_H_

#include <stdint.h>
#include <stdio.h>
#include <errno.h>

#include "timer_functions.h"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

// Copied from http://c.learncodethehardway.org/book/ex20.html
#define log_test_action(M, ...) fprintf(stdout, ANSI_COLOR_GREEN "\t\t\t[ACTION] [%s : %d] " M "\n" ANSI_COLOR_RESET, __FILE__, __LINE__, ##__VA_ARGS__); fflush(stdout);
#define log_test_result(M, ...) fprintf(stdout, ANSI_COLOR_RED "[RESULT] [%s : %d] " M "\n"  ANSI_COLOR_RESET, __FILE__, __LINE__, ##__VA_ARGS__); fflush(stdout);
#define log_test_info(M, ...) fprintf(stdout,"[INFO] [%s : %d] " M "\n", __FILE__, __LINE__, ##__VA_ARGS__); fflush(stdout);

#endif // _LOG_TEST_H_