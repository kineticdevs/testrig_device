#ifndef _MUX_CHANNEL_DEFS_H_
#define _MUX_CHANNEL_DEFS_H_

/* MUX Channels, GND Mux  */
#define JTST_GND_G          (4)
#define JBAT_GND_G          (1)
#define VCHG_GND_G          (14)
#define VBUS2_G             (10)
#define CHIP_SELECT_G       (13)
#define U2_TX_G             (12)
#define U2_RX_G             (11)
#define JMTR_NEG_G          (3)
#define JTST_I2C_SCL_1P8_G  (6)
#define JTST_I2C_SDA_1P8_G  (8)
#define JTST_V1P8_G         (7)
#define JBAT_VBAT_G         (0)
#define JTST_V3P3_E_G       (5)
#define JMTR_POS_G          (2)
#define JTST_VCC_OUT_G      (9)

/* MUX Channels, 3v3 Mux  */
#define JTST_VCC_OUT_H      (6)
#define JMTR_POS_H          (13)
#define JTST_V3P3_E_H       (10)
#define JBAT_VBAT_H         (15)
#define JTST_V1P8_H         (8)
#define JTST_I2C_SDA_1P8_H  (7)
#define JTST_I2C_SCL_1P8_H  (9)
#define JMTR_NEG_H          (12)
#define U2_RX_H             (4)
#define U2_TX_H             (3)
#define CHIP_SELECT_H       (2)
#define VBUS2_H             (5)
#define VCHG_GND_H          (1)
#define JBAT_GND_H          (14)
#define JTST_GND_H          (11)

#endif
