#ifndef _POWER_TEST_H_
#define _POWER_TEST_H_

void toggleBattery(bool enabled);
void toggle5V(bool enabled);
uint8_t powerCheck(int32_t adapter);
// uint8_t usbDetectCheck(int32_t adapter);

#endif
