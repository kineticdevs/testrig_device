#ifndef _SENSOR_TEST_H_
#define _SENSOR_TEST_H_

uint8_t testMPU9150(int32_t adapter);
uint8_t testBMP280(int32_t adapter);

#endif
