#ifndef _TEST_ERROR_DEFS_H_
#define _TEST_ERROR_DEFS_H_

#define SUCCESS                 (0x00)
#define TESTRIG_INIT_FAIL       (0x01)
#define I2C_ADAPTER_FAIL        (0x02)
#define CONTINUITY_CHECK_FAIL   (0x03)
#define DISPLAY_WRITE_FAIL      (0x04)
#define INIT_MPU_FAIL           (0x05)
#define MPU_BAD_VAL_FAIL        (0x06)
#define INIT_BMP_FAIL           (0x07)
#define BMP_BAD_VAL_FAIL        (0x08)
#define MOTOR_FAIL              (0x09)
#define TECHNICIAN_FAIL         (0x0A)
#define RED_LED_FAIL            (0x0B)
#define GREEN_LED_FAIL          (0x0C)
#define BLUE_LED_FAIL           (0x0D)
#define REGULATOR_FAIL          (0x0E)
#define CHARGER_FAIL            (0x0F)
#define CHARGER_REG_FAIL        (0x10)
#define USB_DETECT_FAIL         (0x11)
#define REED_SWITCH_FAIL        (0x12)
#define BUZZER_FAIL	   	 	    (0x13)

#endif
