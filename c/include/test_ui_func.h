#ifndef _TEST_UI_FUNC_H_
#define _TEST_UI_FUNC_H_

/*
 *  test_ui_func.c
 *
 *  Implementations for test UI functions. Each test has
 *  a function that is called on test start, and functions
 *  for indicating pass and fail.
 */

#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <errno.h>
#include "tricolor_led.h"
#include "display.h"

#include "display_utils.h"

void indicateLongPress(void);
void displayStep(uint8_t step);

void defaultCheckStart(void);
void defaultCheckPass(void);
void defaultCheckFail(void);
void defaultCheckNonCritical(void);

/* Continuity check indicators. */
void continuityCheckStart(void);
void continuityCheckPass(void);
void continuityCheckFail(void);

/* Button check indicators */
void buttonCheckPass(void);

/* LCD check indicators. */
void lcdCheckStart(void);
void lcdCheckPass(void);
void lcdCheckFail(void);
/* Power test indicators */
void powerCheckStart(void);
void powerCheckPass(void);
void powerCheckFail(void);
/* LED check indicators. */
void ledCheckStart(void);
void ledCheckPass(void);
void ledCheckFail(void);
/* MPU test indicators */
void mpuCheckStart(void);
void mpuCheckPass(void);
void mpuCheckFail(void);

/* BMP test indicators */
void bmpCheckStart(void);
void bmpCheckPass(void);
void bmpCheckFail(void);

/* Motor test indicators */
void motorCheckStart(void);
void motorCheckPass(void);
void motorCheckFail(void);

/* Buzzer test indicators */
void buzzerCheckStart(void);
void buzzerCheckPass(void);
void buzzerCheckFail(void);

/* Reed switch indicators */
void reedSwitchCheckStart(void);
void reedSwitchCheckPass(void);
void reedSwitchCheckFail(void);

#endif
