#ifndef _TESTRIG_GPIO_DEFS_
#define _TESTRIG_GPIO_DEFS_

#define EN_MUX          114

#define MUX_GND_SEL_0   129
#define MUX_GND_SEL_1   130
#define MUX_GND_SEL_2   131
#define MUX_GND_SEL_3   49

#define MUX_3V3_SEL_0   44
#define MUX_3V3_SEL_1   45
#define MUX_3V3_SEL_2   128
#define MUX_3V3_SEL_3   15

#define USB_IN_PIN			14
#define REED_SWITCH_PIN 48
#define BUTTON_PIN 			46

#define BAT_SEL         109
#define EN_5V           115

#define RESTART_BUTTON  110

#endif
