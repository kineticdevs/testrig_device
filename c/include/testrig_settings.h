#ifndef _TESTRIG_SETTINGS_H_
#define _TESTRIG_SETTINGS_H_

/* Short threshold voltage in A/D counts; 0x7FF = 4.096V */
#define ADC_SHORT_THRESHOLD         (50)    /* 4.096V*50/2047 = 0.100V */
#define ADC_OPEN_THRESHOLD          (1500)  /* 4.096*1500/2047 = 3.00V */
#define BATTERY_VOLTAGE_MIN         (3500)
#define BATTERY_VOLTAGE_MAX         (4000)
#define MAX_ITERATIONS_IMU_CAL      (100)
#define MOTOR_TEST_DURATION_MS      (10000)
#define BUZZER_TEST_DURATION_MS      (10000)
#define REED_TEST_DURATION_MS      (10000)

/* Power test thresholds for VBAT, no charger */
#define VBAT_MIN_NO_CHARGE          (1100)  /* Min=3.3V, 2/3 divider, (3.3*2/3)*2048/4.096 */
#define VBAT_MAX_NO_CHARGE          (1267)  /* Max=3.8V, 2/3 divider, (3.8*2/3)*2048/4.096 */

/* Power test thresholds for VCC_OUT, no charger (should be same as VBAT) */
#define VCCOUT_MIN_NO_CHARGE        (1100)  /* Min=3.3V, 2/3 divider, (3.3*2/3)*2048/4.096 */
#define VCCOUT_MAX_NO_CHARGE        (1267)  /* Max=3.8V, 2/3 divider, (3.8*2/3)*2048/4.096 */

/* Power test thresholds for VBAT, charging */
#define VBAT_MIN_CHARGING           (1366)  /* Min=4.1V, 2/3 divider, (4.1*2/3)*2048/4.096 */
#define VBAT_MAX_CHARGING           (1433)  /* Max=4.3V, 2/3 divider, (4.3*2/3)*2048/4.096 */

/* Power test thresholds for VCC_OUT, charging (should be VBAT+200mV) */
#define VCCOUT_MIN_CHARGING         (1433)  /* Min=4.3V, 2/3 divider, (4.3*2/3)*2048/4.096 */
#define VCCOUT_MAX_CHARGING         (1500)  /* Max=4.5V, 2/3 divider, (4.5*2/3)*2048/4.096 */

/* Pressure sensor - local pressure range in Deerfield, MA */
#define MIN_PRESSURE_PASCAL			 99000.0
#define MAX_PRESSURE_PASCAL			103000.0

/* Temperature range */
#define MIN_TEMP_CELSIUS			 5.0
#define MAX_TEMP_CELSIUS			40.0

#endif
