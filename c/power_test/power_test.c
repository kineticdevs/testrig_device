#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <fcntl.h>

#include "testrig_gpio_defs.h"
#include "mux_channel_defs.h"
#include "test_error_defs.h"
#include "testrig_settings.h"
#include "gpio_pwm.h"
#include "log.h"
#include "i2c_func.h"
#include "power_test.h"
#include "ads1015.h"
#include "timer_functions.h"

#define VBAT_ANALOG_CH    (1)
#define VCCOUT_ANALOG_CH  (2)

static int16_t measureBattery(void);
static int16_t measureVCCOUT(void);

static int16_t measureBattery(void) {
  setInputADC(VBAT_ANALOG_CH);
  usleep(10000);    // allow 10ms to settle, probably overkill
  return readADC();
}

static int16_t measureVCCOUT(void) {
  setInputADC(VCCOUT_ANALOG_CH);
  usleep(10000);    // allow 10ms to settle, probably overkill
  return readADC();
}

void toggleBattery(bool enabled) {
  uint8_t value;
  value = enabled ? 0 : 1;
  gpio_pin_set_value(BAT_SEL, value);
}

void toggle5V(bool enabled) {
  uint8_t value;
  value = enabled ? 1 : 0;
  gpio_pin_set_value(EN_5V, value);
}

uint8_t usbDetectCheck(int32_t adapter) {
    char file_path[64];
    char value;
    int32_t usb_detect_fd = -1;

    sprintf(file_path, "/sys/class/gpio/gpio%d/value", USB_IN_PIN);
    toggleBattery(true);
    toggle5V(false);
    usleep(10000);

    usb_detect_fd = open(file_path, O_RDONLY);
    if (usb_detect_fd == -1) {
      log_error("Couldn't access USB_DETECT pin.")
      toggle5V(false);
      toggleBattery(false);
      return USB_DETECT_FAIL;
    }
    read(usb_detect_fd, &value, 1); // read ASCII value. The value is '0' if button is pressed otherwise '1'
    lseek(usb_detect_fd, 0, SEEK_SET);
    log_info("USB: %d", value-'0');

    toggle5V(true);
    usleep(10000);

    read(usb_detect_fd, &value, 1); // read ASCII value. The value is '0' if button is pressed otherwise '1'
    lseek(usb_detect_fd, 0, SEEK_SET);
    log_info("USB: %d", value-'0');

    toggle5V(false);
    toggleBattery(false);
    return 0;
}

/*
 *    PowerCheck
 *    Run battery/regulator checks.
 */
uint8_t powerCheck(int32_t adapter) {
  int16_t ad_count = 0;
  initADC(adapter);
  usleep(100000);

  /* Turn on battery, give 100ms to settle (overkill) */
  toggleBattery(true);
  toggle5V(false); // should be already false but still force it
  usleep(100000);
  ad_count = measureBattery();
  log_info("VBAT, no charger: %d.", ad_count);
  if ((ad_count < VBAT_MIN_NO_CHARGE) || (ad_count > VBAT_MAX_NO_CHARGE)) {
    log_error("VBAT [%d] out of acceptable range [%d, %d]",ad_count,VBAT_MIN_NO_CHARGE,VBAT_MAX_NO_CHARGE);
    return REGULATOR_FAIL;
  }
  ad_count = measureVCCOUT();
  log_info("VCCOUT, no charger: %d.", ad_count);
  if ((ad_count < VCCOUT_MIN_NO_CHARGE) || (ad_count > VCCOUT_MAX_NO_CHARGE)) {
    log_error("VCCOUT out of acceptable range.");
    return REGULATOR_FAIL;
  }

/* Turn on charger, give 100ms to settle (overkill) */
  toggle5V(true);
  usleep(100000);
  ad_count = measureBattery();
  log_info("VBAT, charging: %d.", ad_count);
  if ((ad_count < VBAT_MIN_CHARGING) || (ad_count > VBAT_MAX_CHARGING)) {
    log_error("VBAT out of acceptable range.");
    return CHARGER_FAIL;
  }
  ad_count = measureVCCOUT();
  log_info("VCCOUT, charging: %d.", ad_count);
  if ((ad_count < VCCOUT_MIN_CHARGING) || (ad_count > VCCOUT_MAX_CHARGING)) {
    log_error("VCCOUT out of acceptable range.");
    return CHARGER_REG_FAIL;
  }

  toggle5V(false);
  toggleBattery(false);
  return SUCCESS;
}
