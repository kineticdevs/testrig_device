#!/bin/sh

# This file is almost identical to /device/c/scripts/export_gpio_pins.sh, with
# some additional pins at the top for configuring the continuity check MUX pins.

echo "" > /home/kinetic/datafiles/.gpiostatus

echo 128 > /sys/class/gpio/export
echo 129 > /sys/class/gpio/export
echo 130 > /sys/class/gpio/export
echo 131 > /sys/class/gpio/export
echo 114 > /sys/class/gpio/export
echo 115 > /sys/class/gpio/export
echo 109 > /sys/class/gpio/export
echo 110 > /sys/class/gpio/export

chmod 777 /sys/class/gpio/gpio128/direction
chmod 777 /sys/class/gpio/gpio128/value
chmod 777 /sys/class/gpio/gpio129/direction
chmod 777 /sys/class/gpio/gpio129/value
chmod 777 /sys/class/gpio/gpio130/direction
chmod 777 /sys/class/gpio/gpio130/value
chmod 777 /sys/class/gpio/gpio131/direction
chmod 777 /sys/class/gpio/gpio131/value
chmod 777 /sys/class/gpio/gpio114/direction
chmod 777 /sys/class/gpio/gpio114/value
chmod 777 /sys/class/gpio/gpio115/direction
chmod 777 /sys/class/gpio/gpio115/value
chmod 777 /sys/class/gpio/gpio109/direction
chmod 777 /sys/class/gpio/gpio109/value
chmod 777 /sys/class/gpio/gpio110/direction
chmod 777 /sys/class/gpio/gpio110/value

echo mode0 > /sys/kernel/debug/gpio_debug/gpio128/current_pinmux
echo mode0 > /sys/kernel/debug/gpio_debug/gpio129/current_pinmux
echo mode0 > /sys/kernel/debug/gpio_debug/gpio130/current_pinmux
echo mode0 > /sys/kernel/debug/gpio_debug/gpio131/current_pinmux
echo mode0 > /sys/kernel/debug/gpio_debug/gpio114/current_pinmux
echo mode0 > /sys/kernel/debug/gpio_debug/gpio115/current_pinmux
echo mode0 > /sys/kernel/debug/gpio_debug/gpio109/current_pinmux
echo mode0 > /sys/kernel/debug/gpio_debug/gpio110/current_pinmux

# EN_5V
echo out > /sys/class/gpio/gpio115/direction
echo 0 > /sys/class/gpio/gpio115/value

# BAT_SEL
echo out > /sys/class/gpio/gpio109/direction
echo 1 > /sys/class/gpio/gpio109/value

# RESTART_BUTTON
echo in > /sys/class/gpio/gpio110/direction

# MUX_EN_GPIO_B
# Output, init low
echo out > /sys/class/gpio/gpio114/direction
echo 0 > /sys/class/gpio/gpio114/value

# MUX_GND Default: Channel 1 (JBAT_GND)
# MUX_GND_SEL_0
# Output, init high, disable UART1 multiplex
echo out > /sys/class/gpio/gpio129/direction
echo 1 > /sys/class/gpio/gpio129/value

# MUX_GND_SEL_1
# Output, init low, disable UART1 multiplex
echo out > /sys/class/gpio/gpio130/direction
echo 0 > /sys/class/gpio/gpio130/value

# MUX_GND_SEL_2
# Output, init low, disable UART1 multiplex
echo out > /sys/class/gpio/gpio131/direction
echo 0 > /sys/class/gpio/gpio131/value

# MUX_GND_SEL_3
# Output, init low
echo 49 > /sys/class/gpio/export
chmod 777 /sys/class/gpio/gpio49/direction
chmod 777 /sys/class/gpio/gpio49/value
echo out > /sys/class/gpio/gpio49/direction
echo 0 > /sys/class/gpio/gpio49/value

# MUX_3V3 Default: Channel 10 (JTST_V3P3)
# MUX_3V3_SEL_0
# Output, init low
echo 44 > /sys/class/gpio/export
chmod 777 /sys/class/gpio/gpio44/direction
chmod 777 /sys/class/gpio/gpio44/value
echo out > /sys/class/gpio/gpio44/direction
echo 0 > /sys/class/gpio/gpio44/value

# MUX_3V3_SEL_1
# Output, init high
echo 45 > /sys/class/gpio/export
chmod 777 /sys/class/gpio/gpio45/direction
chmod 777 /sys/class/gpio/gpio45/value
echo out > /sys/class/gpio/gpio45/direction
echo 1 > /sys/class/gpio/gpio45/value

# MUX_3V3_SEL_2
# Output, init high, disable UART1 multiplex
echo out > /sys/class/gpio/gpio128/direction
echo 1 > /sys/class/gpio/gpio128/value

# MUX_3V3_SEL_3
# Output, init low.
echo 15 > /sys/class/gpio/export
chmod 777 /sys/class/gpio/gpio15/direction
chmod 777 /sys/class/gpio/gpio15/value
echo out > /sys/class/gpio/gpio15/direction
echo 0 > /sys/class/gpio/gpio15/value

echo 12 > /sys/class/gpio/export
chmod 777 /sys/class/gpio/gpio12/direction
chmod 777 /sys/class/gpio/gpio12/value
echo out > /sys/class/gpio/gpio12/direction
#echo mode1 > /sys/kernel/debug/gpio_debug/gpio12/current_pinmux

echo 13 > /sys/class/gpio/export
chmod 777 /sys/class/gpio/gpio13/direction
chmod 777 /sys/class/gpio/gpio13/value
echo out > /sys/class/gpio/gpio13/direction
#echo mode1 > /sys/kernel/debug/gpio_debug/gpio13/current_pinmux

echo 14 > /sys/class/gpio/export
chmod 777 /sys/class/gpio/gpio14/direction
chmod 777 /sys/class/gpio/gpio14/value

# reed switch
echo 48 > /sys/class/gpio/export
chmod 777 /sys/class/gpio/gpio48/direction
chmod 777 /sys/class/gpio/gpio48/value
echo in > /sys/class/gpio/gpio48/direction

echo 182 > /sys/class/gpio/export
chmod 777 /sys/class/gpio/gpio182/direction
chmod 777 /sys/class/gpio/gpio182/value
echo out > /sys/class/gpio/gpio182/direction
#echo mode1 > /sys/kernel/debug/gpio_debug/gpio182/current_pinmux

echo 183 > /sys/class/gpio/export
chmod 777 /sys/class/gpio/gpio183/direction
chmod 777 /sys/class/gpio/gpio183/value
echo out > /sys/class/gpio/gpio183/direction
#echo mode1 > /sys/kernel/debug/gpio_debug/gpio183/current_pinmux

#export the pin for the second button
echo 46 > /sys/class/gpio/export
chmod 777 /sys/class/gpio/gpio46/direction
chmod 777 /sys/class/gpio/gpio46/value
echo in > /sys/class/gpio/gpio46/direction

#export the pin for the buzzer
echo 47 > /sys/class/gpio/export
chmod 777 /sys/class/gpio/gpio47/direction
chmod 777 /sys/class/gpio/gpio47/value
echo out > /sys/class/gpio/gpio47/direction

#export the pin for OLED_RESET
echo 42 > /sys/class/gpio/export
chmod 777 /sys/class/gpio/gpio42/direction
chmod 777 /sys/class/gpio/gpio42/value
echo out > /sys/class/gpio/gpio42/direction
echo mode0 > /sys/kernel/debug/gpio_debug/gpio42/current_pinmux

# echo 0 > /sys/class/pwm/pwmchip0/export
# chmod 777 /sys/class/pwm/pwmchip0/pwm0/duty_cycle
# chmod 777 /sys/class/pwm/pwmchip0/pwm0/period
# chmod 777 /sys/class/pwm/pwmchip0/pwm0/enable

# echo 1 > /sys/class/pwm/pwmchip0/export
# chmod 777 /sys/class/pwm/pwmchip0/pwm1/duty_cycle
# chmod 777 /sys/class/pwm/pwmchip0/pwm1/period
# chmod 777 /sys/class/pwm/pwmchip0/pwm1/enable

# echo 2 > /sys/class/pwm/pwmchip0/export
# chmod 777 /sys/class/pwm/pwmchip0/pwm2/duty_cycle
# chmod 777 /sys/class/pwm/pwmchip0/pwm2/period
# chmod 777 /sys/class/pwm/pwmchip0/pwm2/enable

# echo 3 > /sys/class/pwm/pwmchip0/export
# chmod 777 /sys/class/pwm/pwmchip0/pwm3/duty_cycle
# chmod 777 /sys/class/pwm/pwmchip0/pwm3/period
# chmod 777 /sys/class/pwm/pwmchip0/pwm3/enable

echo mode1 > /sys/kernel/debug/gpio_debug/gpio27/current_pinmux
echo mode1 > /sys/kernel/debug/gpio_debug/gpio28/current_pinmux
chmod 777 /dev/i2c-6

echo "DONE" > /home/kinetic/datafiles/.gpiostatus

#rfkill unblock bluetooth
