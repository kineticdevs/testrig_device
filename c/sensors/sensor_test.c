#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

#include "mpu9150.h"
#include "mpu9150_dmp.h"
#include "bmp280.h"
#include "i2c_func.h"
#include "log.h"
#include "test_error_defs.h"
#include "testrig_settings.h"

#define PRESSURE_0			                      101325.0
#define NUM_AVERAGED_INIT_ALTITUDE_READINGS   20
#define NUM_IGNORED_INIT_ALTITUDE_READINGS    50

static char *calibration_file_loc="/home/kinetic/datafiles/.calibrationconstants";
static int16_t calibration_offsets[6];
static uint8_t dmp_buffer[MPU6050_DMP_PACKET_SIZE];

// Assume DUT is placed face up in test harness.
static int16_t grav_vector_expected[3] = {0, 0, -1};
int32_t i2c_adapter = -1;

typedef struct {
  int16_t accel_X;
  int16_t accel_Y;
  int16_t accel_Z;
  int16_t gyro_X;
  int16_t gyro_Y;
  int16_t gyro_Z;
} imu_reading_t;

typedef struct {
  double temperature;
  double pressure;
  int16_t altitude;
} bmp_reading_t;

static inline bool get_mpu9150_reading(uint8_t *data) {

  bool has_data = false;
	// set the active address in the i2c adapter to the address of the mpu9150
	// all i2c communication will go to the mpu9150
  i2c_set_device_address(i2c_adapter, MPU6050_ADDRESS);
  uint8_t mpuIntStatus = getIntStatus();
	uint16_t fifo_count = getFIFOCount();
  // check for overflow (this should never happen unless our code is too inefficient)
  if ((mpuIntStatus & 0x10) || fifo_count == 1024) {
      // reset so we can continue cleanly
      resetFIFO();
    // otherwise, check for DMP data ready interrupt (this should happen frequently)
  }else if (mpuIntStatus & 0x02) {
//    } else if (mpuIntStatus & 0x01) { // the MPU9150 register manual shows this bit but
										// Jeff Rowberg's library shows 0x02. Apparently that's for DMP
        // wait for correct available data length, should be a VERY short wait
        LABEL_FIFO_DO_AGAIN: while (fifo_count < MPU6050_DMP_PACKET_SIZE){
			fifo_count = getFIFOCount();
			// printf("Waiting... fifo_count = %d\n",fifo_count); fflush(stdout);
		}

		// Found that sometimes FIFO gets corrupted and it doesn't have data in the mutliples of MPU6050_DMP_PACKET_SIZE
		// That happens rarely but we should reset the FIFO if that happens
		if((fifo_count%MPU6050_DMP_PACKET_SIZE) !=0){
			// printf("Waiting for fifo_count = %d to become a multiple\n",fifo_count); fflush(stdout);

			uint8_t i_delay = 0; // To wait for 40ms to get good FIFO count
			bool goodFIFOcount = false;
			for(i_delay=0; i_delay<20; i_delay++){
				usleep(2000); // wait for 2ms to get another sample of data
				fifo_count = getFIFOCount();
				if((fifo_count%MPU6050_DMP_PACKET_SIZE) ==0){
					goodFIFOcount = true;
					break; // exit this for loop
				}
			}
			if(!goodFIFOcount){
				// If it's still not a multiple then reset FIFO
				resetFIFO();
				// printf("Resetting FIFO because fifo_count = %d\n",fifo_count); fflush(stdout);
				usleep(40000); // Wait for 40ms
				fifo_count = getFIFOCount();
				goto LABEL_FIFO_DO_AGAIN;
			}
		}


		uint16_t available_samples = (uint16_t)(fifo_count/MPU6050_DMP_PACKET_SIZE); // ideally should be one but more than one might be available
    has_data = true;
		// Don't worry about any incomplete sample i.e., fifo_count%MPU6050_DMP_PACKET_SIZE

		// Above shows that we have may be more than 42 bytes in the FIFO
		// Now read the available samples to clear the FIFO till we reach the latest packet
		while(available_samples>1){ // i.e., 2 or more complete samples
			// printf("***** Got %d samples available in FIFO, and fifo_count = %d ***** \n", available_samples,fifo_count); fflush(stdout);
			uint8_t skip_buffer[MPU6050_DMP_PACKET_SIZE];
			getFIFOBytes(skip_buffer,MPU6050_DMP_PACKET_SIZE/2);
			getFIFOBytes(skip_buffer+21,MPU6050_DMP_PACKET_SIZE/2);
			available_samples--;
		}

		// Now we are the latest sample
		//getFIFOBytes(dmp_buffer,MPU6050_DMP_PACKET_SIZE); // NOTE: MUST NOT DO THAT -- READING all 42 bytes at once will not read last 10-12 bytes properly
		// Read 14bytes at a time
		getFIFOBytes(dmp_buffer,MPU6050_DMP_PACKET_SIZE/3);
		getFIFOBytes(dmp_buffer+14,MPU6050_DMP_PACKET_SIZE/3);
		getFIFOBytes(dmp_buffer+28,MPU6050_DMP_PACKET_SIZE/3);
	}

    // // track FIFO count here in case there is > 1 packet available
    // fifoCount -= packetSize;

	// For 1MM application, we always want to get the first packet from the FIFO
	// Function as FIFO where we take out the LAST value and then reset FIFO

	//if(fifoCount >= 3*packetSize){
	//resetFIFO(); // BE CAREFUL... cleaning it

	// wait until the DMP shows that data is ready, and then until we have an entire packet in the FIFO
//	while(!(getIntStatus() & 0x02));
//	while(!dmpPacketAvailable());

	uint8_t i, index = 0; 	// The axes index - First 3 are accelerometer, then quaternion and final 3 are gyro

	// package accel -- Copy 2 bytes then skip 2, and then again copy 2 bytes and so on...
	for(i = 28; i < 38; i += 4) {
		memcpy(data+index,&dmp_buffer[i],2);
		index=index+2;
	}

	// package quaternion
	for(i = 0; i < 14; i += 4) {
		memcpy(data+index,&dmp_buffer[i],2);
		index=index+2;
	}

	// package gyro
	for(i = 16; i < 26; i += 4) {
		memcpy(data+index,&dmp_buffer[i],2);
		index=index+2;
	}
  return has_data;
}

uint8_t testMPU9150(int32_t adapter) {
  uint8_t mpu_reading[20];
  uint8_t log_line[128];
  uint8_t *linePtr;
  uint8_t i, j;
  imu_reading_t reading;
  bool has_data = false;

  i2c_adapter = adapter;
  mpu9150_set_adapter_file(i2c_adapter);
  i2c_set_device_address(i2c_adapter, MPU6050_ADDRESS);

  // First setup MPU9150 before calibration
	if(setup_mpu9150(calibration_offsets) < 0) {
		log_error("Failed at setup_mpu9150()");
		return INIT_MPU_FAIL;
	}

  // Test calibration
  if (mpu9150_calibrate(calibration_offsets, MAX_ITERATIONS_IMU_CAL, grav_vector_expected) != 0) {
    log_error("MPU9150 calibration failed to converge.");
    return INIT_MPU_FAIL;
  }
  log_info("Calibration result: %d %d %d %d %d %d\n", calibration_offsets[0], calibration_offsets[1], calibration_offsets[2], calibration_offsets[3], calibration_offsets[4], calibration_offsets[5]);
  if (setup_mpu9150(calibration_offsets) < 0) {
    log_error("Failed to set up with calibration constants.");
    return INIT_MPU_FAIL;
  }

  sleep(5);

  for(j=0; j<50; j++) {
    has_data = get_mpu9150_reading(mpu_reading);
    if (has_data) break;
    usleep(10000);
  }
  if (j==50) return MPU_BAD_VAL_FAIL;

  linePtr = log_line;
  for (i=0; i<20; i++) linePtr += sprintf(linePtr, "%0X ", mpu_reading[i]);
  log_info("raw: %s", log_line);
  reading.accel_X = ((int16_t)mpu_reading[0]<<8)|mpu_reading[1];
  reading.accel_Y = ((int16_t)mpu_reading[2]<<8)|mpu_reading[3];
  reading.accel_Z = ((int16_t)mpu_reading[4]<<8)|mpu_reading[5];
  reading.gyro_X = ((int16_t)mpu_reading[14]<<8)|mpu_reading[15];
  reading.gyro_Y = ((int16_t)mpu_reading[16]<<8)|mpu_reading[17];
  reading.gyro_Z = ((int16_t)mpu_reading[18]<<8)|mpu_reading[19];
  log_info("Got reading from MPU:\n %d %d %d g: %d %d %d", reading.accel_X, reading.accel_Y, reading.accel_Z, reading.gyro_X, reading.gyro_Y, reading.gyro_Z);

  return SUCCESS;

}

// get a set of readings from the bmp280
static inline void get_bmp280_reading(bmp_reading_t *reading) {
	// set the active address in the i2c adapter to the address of the BMP280
	// all i2c communication will go to the BMP280
	i2c_set_device_address(i2c_adapter, BMP280_ADDRESS);

	bmp280_get_compensated_temp_press_double(&(reading->temperature), &(reading->pressure));
	reading->altitude = (int16_t)(100 * 44330.0 * (1 - pow((reading->pressure)/(PRESSURE_0), 1 / 5.225)));

}

uint8_t testBMP280(int32_t adapter) {
  bmp_reading_t bmp_reading;
  uint8_t i;
  int32_t initial_altitude = 0;

  i2c_adapter = adapter;
  bmp280_set_adapter_file(i2c_adapter);
  i2c_set_device_address(i2c_adapter, BMP280_ADDRESS);

  if (bmp280_init() < 0) {
    log_error("Failed to init BMP280.");
    return INIT_BMP_FAIL;
  }

  if (!bmp280_check_connection()) {
    log_error("BMP280 'check connection' failed.");
    return INIT_BMP_FAIL;
  }

  for (i=0; i<NUM_IGNORED_INIT_ALTITUDE_READINGS; i++) {
    get_bmp280_reading(&bmp_reading);
    usleep(10000);
  }

  for (i=0; i<NUM_AVERAGED_INIT_ALTITUDE_READINGS; i++) {
    get_bmp280_reading(&bmp_reading);
    initial_altitude+=bmp_reading.altitude;
    usleep(10000);
  }
  initial_altitude /= i;    // average.

  log_info("Got BMP280 data: P=%f, T=%f, alt=%d, init_alt=%d", bmp_reading.pressure, bmp_reading.temperature, bmp_reading.altitude, initial_altitude);

  bool success = false;
  if((bmp_reading.pressure > MIN_PRESSURE_PASCAL) && (bmp_reading.pressure < MAX_PRESSURE_PASCAL)){
    log_info("Pressure [in Pa]: %f < %f < %f",MIN_PRESSURE_PASCAL,bmp_reading.pressure,MAX_PRESSURE_PASCAL);
    if((bmp_reading.temperature > MIN_TEMP_CELSIUS) && (bmp_reading.temperature < MAX_TEMP_CELSIUS)) {
      log_info("Temperature [in deg. C]: %f < %f < %f",MIN_TEMP_CELSIUS,bmp_reading.temperature,MAX_TEMP_CELSIUS);
      success = true;
    }
  }

  if(!success) return BMP_BAD_VAL_FAIL;

  return SUCCESS;
}
