#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <errno.h>
#include <math.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <fcntl.h>

#include "log.h"
#include "log_test.h"
#include "timer_functions.h"
#include "testrig_gpio_defs.h"
#include "gpio_pwm.h"
#include "testrig_settings.h"
#include "test_ui_func.h"
#include "test_error_defs.h"
#include "i2c_func.h"
#include "display.h"
#include "continuity.h"
#include "sensor_test.h"
#include "vibration_motor.h"
#include "display_utils.h"
#include "power_test.h"

#include "globals.h"

int env_verbosity = 0; // verbosity env variable

// Set as extern in display.h
// Not used in test rig, defined here to prevent compilation errors when using
// display library from device/c.

bool volatile update_display_this_run = false;
bool volatile display_scroll = false;

#define N_MAX_TESTS				(11)		// total number of max tests to run
#define MIN_LCD_TEST	(2)		// index of first test w/ LCD capability
#define BUTTON_PRESS_MINIMUM_MS					60		// min 60ms for keypress debounce
#define BUTTON_LONG_PRESS_DURATION_MS 	3000	// 3 seconds
#define BUTTON_HELD_DURATION_MS					6000	// 6 seconds

static uint8_t file_location_buffer[100]; // Buffer where the GPIO pin file locations will be saved
static int32_t i2c_adapter = -1;
static char *calibration_file_loc="/home/kinetic/datafiles/.calibrationconstants";


// test modes
enum TEST_MODE_e
{
	TEST_ALL = 0,
	TEST_CONTINUITY,
	TEST_FUNCTIONAL
};
uint8_t testmode = TEST_ALL; // default value


/*** Custom Types ***/

// Struct to determine the change in state
struct GPIO_status_s
{
	bool enable; // Set true to enable
	int32_t file_descriptor; // File descriptor once file is opened
	uint8_t current; // current GPIO status
	uint8_t previous; // previous value to determine the change in state
	int meas_interval_ms; 	// Time duration in ms between checking the status
							// Set it 0 to disable
	uint16_t count; // Current count towards reaching interval
	uint16_t duration_of_current_press_ms;	// Keeps a tab of current duration of press in ms
											// Will be zero when button is not pressed
											// NOTE: Maximum value is approx. 65ms
  uint64_t last_event_time;   // for capturing time of events like key transitions
};

typedef struct {
  bool button_held;
  bool short_press;
  bool long_press;
} keypress_flags_t;

typedef struct {
	char description[32];
	void (*ui_start)(void);
	void (*ui_pass)(void);
	void (*ui_fail)(void);
	uint8_t (*test)(int32_t adapter);
} test_t;

/*** Function prototypes ***/
/* The tests must be prototyped before the 'tests' array can be initialized. */
static uint8_t clear_datafiles(void);
static uint8_t setup_reed_detect_pin(void);
static uint8_t setup_button_detect_pin(void);
static uint8_t setup_usb_detect_pin(void);
static bool check_button_status(keypress_flags_t *flags, struct GPIO_status_s *buttonPtr);
static void waitKeyPress(keypress_flags_t **keys, struct GPIO_status_s **key_gpios, uint8_t count);
static void clearKeyPress(keypress_flags_t *keypress_flags);
static uint8_t usbDetectCheck(int32_t adapter);
static uint8_t technicianCheck(int32_t adapter);
static uint8_t lcdCheck(int32_t adapter);
static uint8_t ledCheck(int32_t adapter);
static uint8_t motorCheck(int32_t adapter);
static uint8_t buzzerCheck(int32_t adapter);
static uint8_t reedSwitchCheck(int32_t adapter);
static void initializeTests(test_t *testArray);

test_t continuityTest = {.description="Continuity", .ui_start=&continuityCheckStart, .ui_pass=&continuityCheckPass, .ui_fail=&continuityCheckFail, .test=&continuityCheck};
test_t hiroseTest = {.description="Hirose Connector", .ui_start=&defaultCheckStart, .ui_pass=&defaultCheckPass, .ui_fail=&defaultCheckFail, .test=&technicianCheck};
test_t zifTest = {.description="Zif Connector", .ui_start=&defaultCheckStart, .ui_pass=&defaultCheckPass, .ui_fail=&defaultCheckFail, .test=&technicianCheck};
test_t buttonTest = {.description="Pushbutton on DUT", .ui_start=&defaultCheckStart, .ui_pass=&defaultCheckPass, .ui_fail=&defaultCheckFail, .test=&technicianCheck};
test_t lcdTest = {.description="LCD", .ui_start=&lcdCheckStart, .ui_pass=&lcdCheckPass, .ui_fail=&lcdCheckFail, .test=&lcdCheck};
test_t powerTest = {.description="Power", .ui_start=&powerCheckStart, .ui_pass=&powerCheckPass, .ui_fail=&powerCheckFail, .test=&powerCheck};
test_t usbDetectTest = {.description="USB Detect", .ui_start=&defaultCheckStart, .ui_pass=&defaultCheckPass, .ui_fail=&defaultCheckFail, .test=&usbDetectCheck};
test_t ledTest = {.description="RGB LED", .ui_start=&ledCheckStart, .ui_pass=&ledCheckPass, .ui_fail=&ledCheckFail, .test=&ledCheck};
test_t imuTest = {.description="IMU", .ui_start=&mpuCheckStart, .ui_pass=&mpuCheckPass, .ui_fail=&mpuCheckFail, .test=&testMPU9150};
test_t bmpTest = {.description="BMP", .ui_start=&bmpCheckStart, .ui_pass=&bmpCheckPass, .ui_fail=&bmpCheckFail, .test=&testBMP280};
test_t motorTest = {.description="Motor", .ui_start=&motorCheckStart, .ui_pass=&motorCheckPass, .ui_fail=&motorCheckFail, .test=&motorCheck};
test_t buzzerTest = {.description="Buzzer", .ui_start=&buzzerCheckStart, .ui_pass=&buzzerCheckPass, .ui_fail=&buzzerCheckFail, .test=&buzzerCheck};
test_t reedSwitchTest = {.description="Reed Switch", .ui_start=&reedSwitchCheckStart, .ui_pass=&reedSwitchCheckPass, .ui_fail=&reedSwitchCheckFail, .test=&reedSwitchCheck};

// Initialize calibration button to check as soon as it's pressed
static struct GPIO_status_s restart_button = {.enable = true, .file_descriptor = -1, .current = '1', .previous = '1', .meas_interval_ms = 0, .count = 0, .duration_of_current_press_ms = 0};
static struct GPIO_status_s button = {.enable = true, .file_descriptor = -1, .current = '1', .previous = '1', .meas_interval_ms = 0, .count = 0, .duration_of_current_press_ms = 0};
static struct GPIO_status_s reed = {.enable = true, .file_descriptor = -1, .current = '1', .previous = '1', .meas_interval_ms = 0, .count = 0, .duration_of_current_press_ms = 0};
static struct GPIO_status_s USB = {.enable = true, .file_descriptor = -1, .current = '1', .previous = '1', .meas_interval_ms = 100, .count = 0};


static void hw_specific_vars(unsigned long rev)
{
	switch(rev){
		case 0x070B: // Or 1803 in decimal
			display_hw_config = DISPLAY_ALT_CONFIG;
			display_rotation = DISPLAY_180_DEG;
			break;
		case 0x090D: // Or 2317 in decimal
			display_hw_config = DISPLAY_ALT_CONFIG;
			display_rotation = DISPLAY_0_DEG;
			break;
		default:
			display_hw_config = DISPLAY_SEQ_CONFIG;
			display_rotation = DISPLAY_0_DEG;
			break;
	}
}

static int parse_input_args(int argc, char *argv[])
{

	if(argc == 1){
		return 0;
	}

	if(strcmp(argv[1], "-help") == 0){
		log_test_info("Input arguments of uart:");
		log_test_info("-mode=?: \n\t\t\t\t?=dev, prod, demo");
		log_test_info("-ble=?: \n\t\t\t\t?=0,1 to disable,enable BLE");
		log_test_info("-vib=?: \n\t\t\t\t?=0,1 to disable,enable vibration");
		log_test_info("-hw=?: \n\t\t\t\t?=1803 for the HW revision in summer of 2016");
		log_test_info("-v=?: \n\t\t\t\t?=0 to 10 the verbosity level");
		return -1;
	}

	char *key, *val;
	int iargc;

	iargc = 1; // because the zeroth argument is name of the function
	while(iargc<argc){

		key = strtok(argv[iargc], "=");
		val = strtok(NULL, "");

		if(strcmp(key, "-v") == 0){
			env_verbosity = strtol(val,0,10);
		}else if(strcmp(key, "-hw") == 0){
			hw_specific_vars(strtoul(val,NULL,0));
		}else if(strcmp(key, "-test") == 0){
			if(strcmp(val, "all") == 0){
				testmode = TEST_ALL;					
			}else if(strcmp(val, "functional") == 0){
				testmode = TEST_FUNCTIONAL;					
			}else if(strcmp(val, "continuity") == 0){
				testmode = TEST_CONTINUITY;					
			}else{
				testmode = TEST_ALL;					
				log_warn("Unknown test type, using default mode: all");
			}
		}else{
			log_warn("Unknown argument: %s", argv[iargc]);
		}
		iargc++;
	}

	return 0;
}


static bool check_button_status(keypress_flags_t *flags, struct GPIO_status_s *buttonPtr)
{
	/*
	If button is pressed, button_pin_file will have 0 in it. Otherwise it is 1.
	*/
  uint64_t current_time_ms = get_time_ms();

	bool key_pressed = false;

	read(buttonPtr->file_descriptor, &(buttonPtr->current), 1); // read ASCII value. The value is '0' if button is pressed otherwise '1'
	lseek(buttonPtr->file_descriptor, 0, SEEK_SET);

	if(buttonPtr->current == '0' && buttonPtr->previous == '1') { // i.e., just pressed
		// Reset the duration of long presses
		buttonPtr->duration_of_current_press_ms = 0;
	buttonPtr->last_event_time = current_time_ms;

	}else if(buttonPtr->current == '0' && buttonPtr->previous == '0'){ // i.e., button is continue to be pressed

		// First increment the duration of long presses
		buttonPtr->duration_of_current_press_ms = current_time_ms - buttonPtr->last_event_time;

		if(buttonPtr->duration_of_current_press_ms >= BUTTON_HELD_DURATION_MS){
			// Code here for held button events
			key_pressed = true;
	  flags->button_held = true;
		}else if (buttonPtr->duration_of_current_press_ms >= BUTTON_LONG_PRESS_DURATION_MS) {
			indicateLongPress();
		} else {
			// Show time when button is pressed instead of showing "Calibrating..."
		}

	}else if(buttonPtr->current == '1' && buttonPtr->previous == '0'){ // i.e., just released
	// Update press interval for last time.
	buttonPtr->duration_of_current_press_ms = current_time_ms - buttonPtr->last_event_time;
	// Then remember time of event.
	buttonPtr->last_event_time = current_time_ms;

		if(buttonPtr->duration_of_current_press_ms > BUTTON_PRESS_MINIMUM_MS) {
			key_pressed = true;
			if (buttonPtr->duration_of_current_press_ms < BUTTON_LONG_PRESS_DURATION_MS) {
				flags->short_press = true;
			}else{
				flags->long_press = true;
			}
		}
		// Reset the duration of long presses
		buttonPtr->duration_of_current_press_ms = 0;

	}else{
		// Button is not pressed, so do nothing
	}

	buttonPtr->previous = buttonPtr->current; // assign current to previous
	return key_pressed;
}

static uint8_t clear_datafiles(void) {
	uint8_t rv = 0;
	log_test_info("Clearing datafiles...");
	log_test_info("	Removing callibration readings...");
	if (access(calibration_file_loc, F_OK) != -1) {
		if (remove(calibration_file_loc)) {
			log_test_info("Unable to delete sensor calibration file.");
			rv = 1;
		}
	}

	return rv;
}

static uint8_t usbDetectCheck(int32_t adapter) {
    char value;
    int32_t usb_detect_fd = -1;

    toggleBattery(false); // don't need battery for this

    toggle5V(false);
    usleep(200000);

	read(USB.file_descriptor, &(USB.current), 1); // read ASCII value. The value is '0' if button is pressed otherwise '1'
	lseek(USB.file_descriptor, 0, SEEK_SET);

	if (USB.current != '1') {
		toggleBattery(false);
		// log_error("USB detected when none present.");
		// return USB_DETECT_FAIL;

		// DONE only because one of the test jigs was failiing this test because of bad resistors between the VBUS and 5V of QP1
		log_warn("Non-critical error: USB detected when none present.");
	}
    log_test_info("USB: %d", USB.current-'0');

    toggle5V(true);
    usleep(200000);

    read(USB.file_descriptor, &(USB.current), 1); // read ASCII value. The value is '0' if button is pressed otherwise '1'
    lseek(USB.file_descriptor, 0, SEEK_SET);
	if (USB.current != '0') {
		log_error("USB not detected when present.");
		return USB_DETECT_FAIL;
	}
	log_test_info("USB: %d", value-'0');

    toggle5V(false);
    return 0;
}

/*
 *	NOTE: Neil switched the return values from i2c.c!
 */
static uint8_t setup_usb_detect_pin() {
	// set up USB detect pin
	memset(file_location_buffer,0,sizeof(file_location_buffer));
	sprintf(file_location_buffer, "/sys/class/gpio/gpio%d/value", USB_IN_PIN);
	gpio_pin_set_direction(USB_IN_PIN, PIN_DIR_IN);
	USB.file_descriptor = open(file_location_buffer, O_RDONLY);
	if(USB.file_descriptor < 0) {
		return 1;
	}
	return 0;
}

/*
 *	NOTE: Neil switched the return values from i2c.c!
 */
static uint8_t setup_button_detect_pin(void) {
	// set up button detect pin
	memset(file_location_buffer,0,sizeof(file_location_buffer));
	sprintf(file_location_buffer, "/sys/class/gpio/gpio%d/value", BUTTON_PIN);
	gpio_pin_set_direction(BUTTON_PIN, PIN_DIR_IN);
	button.file_descriptor = open(file_location_buffer, O_RDONLY);
	if(button.file_descriptor < 0) {
		return 1;
	}
	return 0;
}

/*
 *	NOTE: Neil switched the return values from i2c.c!
 */
static uint8_t setup_restart_button_detect_pin(void) {
	// set up button detect pin
	memset(file_location_buffer,0,sizeof(file_location_buffer));
	sprintf(file_location_buffer, "/sys/class/gpio/gpio%d/value", RESTART_BUTTON);
	gpio_pin_set_direction(RESTART_BUTTON, PIN_DIR_IN);
	restart_button.file_descriptor = open(file_location_buffer, O_RDONLY);
	if(restart_button.file_descriptor < 0) {
		return 1;
	}
	return 0;
}

/*
 *	NOTE: Neil switched the return values from i2c.c!
 */
static uint8_t setup_reed_detect_pin() {
	// set up the reed switch pin
	memset(file_location_buffer,0,sizeof(file_location_buffer)); // Clear the location buffer
	sprintf(file_location_buffer, "/sys/class/gpio/gpio%d/value", REED_SWITCH_PIN);
	gpio_pin_set_direction(REED_SWITCH_PIN, PIN_DIR_IN);
	reed.file_descriptor = open(file_location_buffer, O_RDONLY);
	if(reed.file_descriptor < 0) {
		return 1;
	}
	return 0;
}

static void waitKeyPress(keypress_flags_t **keys, struct GPIO_status_s **key_gpios, uint8_t count) {
	uint8_t i;
	bool key_pressed = false;
	for(i=0; i<count; i++) {
		clearKeyPress(keys[i]);
	}
	while(1) {
		for(i=0; i<count; i++) {
			key_pressed = key_pressed | check_button_status(keys[i], key_gpios[i]);
		}
		if (key_pressed) break;
		usleep(20000);		// sample every 20ms
	}
}

static void clearKeyPress(keypress_flags_t *keypress_flags) {
	keypress_flags->button_held = false;
	keypress_flags->short_press = false;
	keypress_flags->long_press = false;
}

static uint8_t await_startup_scripts(void) {

  uint8_t gpio_char = 0x00;
  int32_t gpio_status_fp = open("/home/kinetic/datafiles/.gpiostatus", O_RDONLY);

  if (gpio_status_fp < 0) return 1;

  while(1) {
	lseek(gpio_status_fp, 0, SEEK_SET);
	read(gpio_status_fp, &gpio_char, 1);
	if (gpio_char == 'D') break;
	sleep(1);
  }

  return 0;
}

static uint8_t initializeTestRig(void) {
  uint8_t rv;

  rv = await_startup_scripts();
  if (rv != 0) {
	log_error("Error opening GPIO status.");
	return rv;
  }

	rv = clear_datafiles();
	if (rv != 0) {
		log_error("Error removing old data files.");
		return rv;
	}

	if ((i2c_adapter = i2c_open_adapter(ADAPTER_NUMBER)) < 0) {
		log_error("Failed to open I2C adapter.");
		return I2C_ADAPTER_FAIL;
	}

  rv = setup_button_detect_pin();
  if (rv != 0) {
	log_error("Error getting button file descriptor.");
	return rv;
  }

  rv = setup_restart_button_detect_pin();
  if (rv != 0) {
	log_error("Error getting restart button file descriptor.");
	return rv;
  }

	rv = setup_reed_detect_pin();
	if (rv != 0) {
	log_error("Error getting reed switch file descriptor.");
	return rv;
  }

	rv = setup_usb_detect_pin();
	if (rv != 0) {
	log_error("Error getting usb detect file descriptor.");
	return rv;
  }

  rv = initContinuityCheck(i2c_adapter);
  if (rv != 0) {
	log_error("Error initializing GPIOs.");
	return rv;
  }
  return rv;
}

static void initializeTestArray(test_t *testArray, uint8_t *n) {

	switch (testmode){
		case TEST_ALL:
			testArray[0] = continuityTest;
			testArray[1] = lcdTest;
			testArray[2] = buttonTest;
			testArray[3] = powerTest;
			testArray[4] = reedSwitchTest;
			testArray[5] = ledTest;
			testArray[6] = imuTest;
			testArray[7] = bmpTest;
			testArray[8] = motorTest;
			testArray[9] = buzzerTest;
			testArray[10] = usbDetectTest;
			*n = 11;
			break;

		case TEST_CONTINUITY:
			testArray[0] = continuityTest;
			*n = 1;
			break;

		case TEST_FUNCTIONAL:
			testArray[0] = lcdTest;
			testArray[1] = buttonTest;
			testArray[2] = powerTest;
			testArray[3] = reedSwitchTest;
			testArray[4] = ledTest;
			testArray[5] = imuTest;
			testArray[6] = bmpTest;
			testArray[7] = motorTest;
			testArray[8] = buzzerTest;
			testArray[9] = usbDetectTest;
			*n = 10;
			break;

	}

/*
	testArray[0] = usbDetectTest;
	testArray[1] = buttonTest;
	testArray[2] = lcdTest;
	testArray[3] = motorTest;
	testArray[4] = ledTest;
	testArray[5] = imuTest;
	testArray[6] = bmpTest;
	testArray[7] = reedSwitchTest;
	testArray[8] = powerTest;
	testArray[9] = reedSwitchTest;
*/
}

/* Tests */

/*
 *	LCD Check--fails if can't open I2C, error on display write.
 */
static uint8_t lcdCheck(int32_t adapter) {
	set_display_i2c_socket(i2c_adapter);
	display_begin();
	sleep(3);
	display_print_message("Testing LCD...", 10, 10, 1, true);
	if (display_update() < 0) return DISPLAY_WRITE_FAIL;
	return SUCCESS;
}

/*
 *	Technician Check
 *	Technician signals "OK" with short press, "FAIL" with long.
 */
 static uint8_t technicianCheck(int32_t adapter) {
	 uint8_t result;
	 keypress_flags_t keypress;
	 keypress_flags_t *keypressPtr = &keypress;
	 struct GPIO_status_s *gpioPtr = &button;
	 clearKeyPress(&keypress);
	 waitKeyPress(&keypressPtr, &gpioPtr, 1);
	 if (keypress.short_press) {
		 return SUCCESS;
	 }
	 return TECHNICIAN_FAIL;
 }

static uint8_t ledCheck(int32_t adapter) {
	tricolor_led_set_color(255, 0, 0);
	bigText("RED?");
	if (technicianCheck(0) != SUCCESS) {
		log_error("Red LED failed.");
		return RED_LED_FAIL;
	}
	tricolor_led_set_color(0, 255, 0);
	bigText("GREEN?");
	if (technicianCheck(0) != SUCCESS) {
		log_error("Green LED failed.");
		return GREEN_LED_FAIL;
	}
	tricolor_led_set_color(0, 0, 255);
	bigText("BLUE?");
	if (technicianCheck(0) != SUCCESS) {
		log_error("Blue LED failed.");
		return BLUE_LED_FAIL;
	}

	return SUCCESS;
}
/*
 *	Motor Check
 */
static uint8_t motorCheck(int32_t adapter) {

	// Need to get 5V or battery on the board to supply power to the motor
	// because it's powered from VCC_OUT
	toggle5V(true);

	uint64_t now, end = 0;
	int32_t time_remaining, time_elapsed;
	uint16_t countdown = 0;
	keypress_flags_t keypress;
	vibration_motor_init();
	now = get_time_ms();
	end = now + MOTOR_TEST_DURATION_MS;
	clearKeyPress(&keypress);

	uint8_t on_cycles = 10; // on for 200ms
	uint8_t off_cycles = 40; // off for 800ms
	uint8_t count = 0;

	bool success = false;
	while(now <= end && !success) {

		count++;
		if(count == 1){
			time_remaining = end-now;
			displayCount(time_remaining/1000);
			vibration_motor_start();
		}else if(count == on_cycles){
			vibration_motor_stop();			
		}else if(count >= on_cycles + off_cycles){
			count = 0;			
		}

		usleep(20000);

		if (check_button_status(&keypress, &button)) success = true;

		now = get_time_ms();
	}
	time_remaining = end-now;
	time_elapsed = MOTOR_TEST_DURATION_MS - time_remaining;

	vibration_motor_stop();
	toggle5V(false);

	if (!success) {
		log_error("Time expired without keypress in motor test.");
		return MOTOR_FAIL;
	}
	log_test_info("Motor test passed in %d ms.", time_elapsed);
	return SUCCESS;
}

/*
 *	Buzzer Check
 */
static uint8_t buzzerCheck(int32_t adapter) {

	// setup the buzzer pin @ pin #47
	int32_t buzzer_pin = -1;
	buzzer_pin = open("/sys/class/gpio/gpio47/value", O_WRONLY);
	if(buzzer_pin < 0) {
		printf("error opening buzzer pin value file\n");
		exit(1);
	}

	// vibrate the buzzer at a frequency of 4kHz, 50% duty cycle
	// 4kHz = 4000 cycles/second = .25 ms per cycle = 250 us
	int buzzer_period_us = 250;
	int buzzer_halfperiod_us = 0.5*buzzer_period_us;	
	int current_clock_state = 0; // High or low

	uint64_t now, end = 0;
	int32_t time_remaining, time_elapsed;
	uint16_t countdown = 0;
	keypress_flags_t keypress;
	now = get_time_ms();
	end = now + BUZZER_TEST_DURATION_MS;
	clearKeyPress(&keypress);

	bool success = false;
	uint64_t time_to_sec = 0;
	while(now <= end && !success) {

		current_clock_state = (current_clock_state > 0) ? 0 : 1;
		if(current_clock_state == 0){
			write(buzzer_pin, "0", 1);		
		}else{
			write(buzzer_pin, "1", 1);		
		}
		lseek(buzzer_pin, SEEK_SET, 0);

		if (check_button_status(&keypress, &button)) success = true;

		usleep(buzzer_halfperiod_us);

		// Only for display
		time_to_sec = time_to_sec + buzzer_halfperiod_us;
		if(time_to_sec>1e6){
			time_remaining = end-now;
			displayCount(time_remaining/1000);
			time_to_sec = 0;
		}
		now = get_time_ms();
	}
	time_remaining = end-now;
	time_elapsed = BUZZER_TEST_DURATION_MS - time_remaining;

	write(buzzer_pin, "0", 1);		

	if (!success) {
		log_error("Time expired without keypress in buzzer test.");
		return BUZZER_FAIL;
	}
	log_test_info("Buzzer test passed in %d ms.", time_elapsed);
	return SUCCESS;
}

/*
 *	Reed switch test
 *	Open and close the switch at least 2 times each	in 10 seconds
 */
static uint8_t reedSwitchCheck(int32_t adapter) {
	char file_path[64];
	char value_c, value_p;
	int32_t reed_switch_fd = -1;

	sprintf(file_path, "/sys/class/gpio/gpio%d/value", REED_SWITCH_PIN);

	reed_switch_fd = open(file_path, O_RDONLY);
	if (reed_switch_fd == -1) {
	  log_error("Couldn't access REED_SWITCH pin.")
	  return REED_SWITCH_FAIL;
	}

	uint64_t now, end = 0;
	keypress_flags_t keypress;
	now = get_time_ms();
	end = now + REED_TEST_DURATION_MS;
	clearKeyPress(&keypress);

	log_test_info("Move magnet in/out at least 3 times");

	display_print_message("Move magnet in/out", 10, 3, 1, true);
	display_print_message("at least 3 times.", 10, 17, 1, false);
	display_update();

	// initialized to some random non '0' and '1' values
	value_c = 'A';
	value_p = 'B';

	char reed_openclose[] = "Opn/Clse: 0/0";
	bool success = false;
	uint8_t count_open = 0;
	uint8_t count_close = 0;
	while(now <= end && !success) {

		read(reed_switch_fd, &value_c, 1); // read ASCII value. The value is '0' if button is pressed otherwise '1'
		lseek(reed_switch_fd, 0, SEEK_SET);

		if (value_c == '0' && value_p == '1') { // i.e., just closed
			count_close++;
			sprintf(reed_openclose,"Opn/Clse: %d/%d",count_open,count_close);
			bigText(reed_openclose);
		}else if (value_c == '1' && value_p == '0') { // i.e., just opened
			count_open++;
			sprintf(reed_openclose,"Opn/Clse: %d/%d",count_open,count_close);
			bigText(reed_openclose);
		}else{
			// do nothing
		}
		value_p = value_c;

		if(count_open > 2 && count_close > 2){
			success = true;
		}

		usleep(100000); // wait for 100ms
		now = get_time_ms();
	}

	if(!success){
		log_error("Reed switch malfunction");
		return REED_SWITCH_FAIL;
	}
/*
	if (technicianCheck(0) != SUCCESS) {
		return REED_SWITCH_FAIL;
	}

	read(reed_switch_fd, &value, 1); // read ASCII value. The value is '0' if button is pressed otherwise '1'
	lseek(reed_switch_fd, 0, SEEK_SET);
	if (value != '0') {
		log_error("Reed switch didn't close.");
		return REED_SWITCH_FAIL;
	}
*/
	return SUCCESS;
}

int main(int argc, char *argv[]) {
	unsigned char test_step, test_result;
	test_t tests[N_MAX_TESTS];
	test_t *testPtr;
	keypress_flags_t *keypressPtr;	// for faking WaitKeyPress with single key
	struct GPIO_status_s *gpioPtr;					//

	keypress_flags_t keypress_flags = {.button_held = false, .short_press = false, .long_press = false};
	keypress_flags_t restart_flags =  {.button_held = false, .short_press = false, .long_press = false};

	keypress_flags_t* keys[2] = {&keypress_flags, &restart_flags};

	struct GPIO_status_s* key_gpios[2] = {&button, &restart_button};

	if(parse_input_args(argc, argv) != 0) {
		log_error("showing help args and terminating\n");
		return 0;
	}

  if (initializeTestRig()) {
		log_error("Couldn't initialize test rig.");
		return TESTRIG_INIT_FAIL;    /* failed to boot program. */
  }
 	log_test_info("\n\n Starting test. \n\n");

 	uint8_t nTests = N_MAX_TESTS; // initialize to all the tests
	initializeTestArray(tests, &nTests);

	for (test_step=0; test_step<nTests; test_step++) {
		if (test_step == 0) {
			if(testmode != TEST_FUNCTIONAL){ // i.e., first test is continuity
				log_test_action("Press START button to when DUT is in place to begin CONTINUITY CHECK.");
			}else{
				log_test_action("Press START button to when DUT is in place to begin FUNCTIONAL TESTS.");
			}
			keypressPtr = &restart_flags;
			gpioPtr = &restart_button;
			display_print_message("Press START button", 10, 10, 1, true);
			waitKeyPress(&keypressPtr, &gpioPtr, 1);
		} else {
			log_test_action("Press OK button to begin next test.");
			// display_print_message("Press OK button", 10, 10, 1, true);
			waitKeyPress(keys, key_gpios, 2);
			if (restart_flags.button_held || restart_flags.short_press || restart_flags.long_press){
				return 1; // exit from the code so that bash script can run it again
			}
			if (keypress_flags.button_held) break;
			if (keypress_flags.long_press) break;
		}
		/* else must have short press, go on to next test */
		if(testmode == TEST_FUNCTIONAL){
			displayStep(test_step+1); // because first step is LCD step			
		}else{
			if (test_step >= MIN_LCD_TEST) {
				displayStep(test_step+1); // because first step is LCD step			
			}
		}
		testPtr = &tests[test_step];
		log_test_info("Beginning test %d: %s.", test_step+1, &(testPtr->description));
		(testPtr->ui_start)();
		test_result = (*(testPtr->test))(i2c_adapter);
		if (test_result != 0) {
			(testPtr->ui_fail)();
			break;
		} else {
			// Special case: if LCD just came up, add step to display.
			if (test_step >= MIN_LCD_TEST-1) displayStep(test_step+1);
			log_test_info("Passed test %d.", test_step+1);
			(testPtr->ui_pass)();
		}
	}
	if (test_step < nTests) {
		if (test_step >= MIN_LCD_TEST-1) displayStep(test_step+1);
		log_test_result("Failed on test %d", test_step+1);
		displayFailed();
	} else {
		log_test_result("All tests passed.");
		displayAllGood();
	}
	log_test_info("Press START button to initiate tests on the next DUT", 10, 10, 1, true);
	waitKeyPress(keys, key_gpios, 2);
	if (test_step >= MIN_LCD_TEST-1) {
		display_buffer_clear();
		display_update();
	}
  return test_result;
}
