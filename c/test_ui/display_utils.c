#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "display.h"
#include "display_utils.h"
#include "log.h"

#define BIG_TEXT_MAX    7
#define BIG_LETTER_WIDTH  18
#define BIG_LETTER_HEIGHT 24

void displayCount(uint16_t count) {
  char count_str[8];
  sprintf(count_str, "%d s", count);
  bigText(count_str);
}

void bigText(char *text) {
/*
  uint8_t length, x, y;
  char out[BIG_TEXT_MAX+1];
  out[BIG_TEXT_MAX] = 0;
  length = (strlen(text) > BIG_TEXT_MAX) ? BIG_TEXT_MAX : strlen(text);
  sprintf(out, "%.7s", text);
  x = (SSD1306_LCDWIDTH - (length*BIG_LETTER_WIDTH)) / 2;
  y = (SSD1306_LCDHEIGHT - BIG_LETTER_HEIGHT) / 2;
  display_print_message(out, x, y, 3, true);
  display_update();
*/
  display_print_message(text, MSG_OFFSET_X, MSG_OFFSET_Y, 2, true);
  display_update();
}
