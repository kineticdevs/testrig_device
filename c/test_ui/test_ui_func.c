/*
 *  test_ui_func.c
 *
 *  Implementations for test UI functions. Each test has
 *  a function that is called on test start, and functions
 *  for indicating pass and fail.
 */

#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <errno.h>
#include "test_ui_func.h"
#include "log.h"

static uint8_t yellow[3] = {128, 128, 0};
static uint8_t green[3] = {0, 255, 0};
static uint8_t red[3] = {255, 0, 0};
static uint8_t blue[3] = {0, 0, 255};

void indicateLongPress(void) {
  tricolor_led_turn_off();
  usleep(20000);
  tricolor_led_turn_on();
  usleep(20000);
  tricolor_led_turn_off();
  usleep(20000);
  tricolor_led_turn_on();
}

void displayFailed(void) {
  display_print_message((char*)"FAILED", MSG_OFFSET_X, MSG_OFFSET_Y, 2, false);
  display_update();
}

void displayAllGood(void) {
  bigText("ALL OK");
}

void displayStep(uint8_t step) {
  char step_str[4];
  sprintf(step_str, "%d.", step);
  display_print_message(step_str, STEP_NUM_OFFSET_X, STEP_NUM_OFFSET_Y, 3, true);
}

void defaultCheckStart(void) {
  tricolor_led_set_color_from_array(yellow);
}

void defaultCheckPass(void) {
  tricolor_led_set_color_from_array(green);
}

void defaultCheckFail(void) {
  tricolor_led_set_color_from_array(red);
  sleep(1);
}

void defaultCheckNonCritical(void) {
  tricolor_led_set_color_from_array(green);
  sleep(1);
}

/* Button check indicators */
void buttonCheckPass(void) {
  tricolor_led_set_color_from_array(green);
  sleep(1);
  tricolor_led_set_color_from_array(blue);
  sleep(3);
  tricolor_led_set_color_from_array(green);
  sleep(1);
}

/* Continuity check indicators. */
void continuityCheckStart(void) {
  defaultCheckStart();
}

void continuityCheckPass(void) {
  defaultCheckPass();
}

void continuityCheckFail(void) {
  defaultCheckFail();
}

/* LCD check indicators. */
void lcdCheckStart(void) {
  tricolor_led_set_color_from_array(blue);
}

void lcdCheckPass(void) {
  display_print_message((char*)"LCD GOOD", MSG_OFFSET_X, MSG_OFFSET_Y, 2, false);
  display_update();
  sleep(1);
}

void lcdCheckFail(void) {
  tricolor_led_set_color_from_array(red);
}

/* Power test indicators */
void powerCheckStart(void) {
  display_print_message((char*)"TESTING POWER", MSG_OFFSET_X, MSG_OFFSET_Y, 2, false);
  tricolor_led_set_color_from_array(yellow);
  display_update();
}

void powerCheckPass(void) {
  display_print_message((char*)"POWER OK", MSG_OFFSET_X, MSG_OFFSET_Y, 2, false);
  tricolor_led_set_color_from_array(green);
  display_update();
}

void powerCheckFail(void) {
  display_print_message((char*)"FAILED", MSG_OFFSET_X, MSG_OFFSET_Y, 2, false);
  tricolor_led_set_color_from_array(red);
  display_update();
}

/* LED test indicators */
void ledCheckStart(void) {
  display_print_message((char*)"TESTING LED", MSG_OFFSET_X, MSG_OFFSET_Y, 2, false);
  tricolor_led_set_color_from_array(yellow);
  display_update();
}

void ledCheckPass(void) {
  display_print_message((char*)"LED OK", MSG_OFFSET_X, MSG_OFFSET_Y, 2, false);
  tricolor_led_set_color_from_array(green);
  display_update();
}

void ledCheckFail(void) {
  display_print_message((char*)"LED FAILED", MSG_OFFSET_X, MSG_OFFSET_Y, 2, false);
  tricolor_led_set_color_from_array(red);
  display_update();
}

/* MPU test indicators */
void mpuCheckStart(void) {
  display_print_message((char*)"TESTING IMU...", MSG_OFFSET_X, MSG_OFFSET_Y, 2, false);
  tricolor_led_set_color_from_array(yellow);
  display_update();
  sleep(1);
}

void mpuCheckPass(void) {
  display_print_message((char*)"IMU OK", MSG_OFFSET_X, MSG_OFFSET_Y, 2, false);
  tricolor_led_set_color_from_array(green);
  display_update();
  sleep(1);
}

void mpuCheckFail(void) {
  display_print_message((char*)"IMU BAD", MSG_OFFSET_X, MSG_OFFSET_Y, 2, false);
  tricolor_led_set_color_from_array(red);
  display_update();
  sleep(1);
}

/* BMP test indicators */
void bmpCheckStart(void) {
  display_print_message((char*)"TESTING BMP...", MSG_OFFSET_X, MSG_OFFSET_Y, 2, false);
  tricolor_led_set_color_from_array(yellow);
  display_update();
  sleep(1);
}

void bmpCheckPass(void) {
  display_print_message((char*)"BMP OK", MSG_OFFSET_X, MSG_OFFSET_Y, 2, false);
  tricolor_led_set_color_from_array(green);
  display_update();
  sleep(1);
}

void bmpCheckFail(void) {
  display_print_message((char*)"BMP BAD", MSG_OFFSET_X, MSG_OFFSET_Y, 2, false);
  tricolor_led_set_color_from_array(red);
  display_update();
  sleep(1);
}

/* Motor test indicators */
void motorCheckStart(void) {
  display_print_message((char*)"TESTING MOTOR", MSG_OFFSET_X, MSG_OFFSET_Y, 2, false);
  tricolor_led_set_color_from_array(yellow);
  display_update();
  sleep(1);
}

void motorCheckPass(void) {
  display_print_message((char*)"MOTOR OK", MSG_OFFSET_X, MSG_OFFSET_Y, 2, false);
  tricolor_led_set_color_from_array(green);
  display_update();
  sleep(1);
}

void motorCheckFail(void) {
  display_print_message((char*)"MOTOR BAD", MSG_OFFSET_X, MSG_OFFSET_Y, 2, false);
  tricolor_led_set_color_from_array(red);
  display_update();
  sleep(1);
}

/* Buzzer test indicators */
void buzzerCheckStart(void) {
  display_print_message((char*)"TESTING BUZZER", MSG_OFFSET_X, MSG_OFFSET_Y, 2, false);
  tricolor_led_set_color_from_array(yellow);
  display_update();
  sleep(1);
}

void buzzerCheckPass(void) {
  display_print_message((char*)"BUZZER OK", MSG_OFFSET_X, MSG_OFFSET_Y, 2, false);
  tricolor_led_set_color_from_array(green);
  display_update();
  sleep(1);
}

void buzzerCheckFail(void) {
  display_print_message((char*)"BUZZER BAD", MSG_OFFSET_X, MSG_OFFSET_Y, 2, false);
  tricolor_led_set_color_from_array(red);
  display_update();
  sleep(1);
}

/* Reed switch test indicators */
void reedSwitchCheckStart(void) {
  display_print_message((char*)"TESTING REED", MSG_OFFSET_X, MSG_OFFSET_Y, 2, false);
  tricolor_led_set_color_from_array(yellow);
  display_update();
  sleep(1);
}

void reedSwitchCheckPass(void) {
  display_print_message((char*)"REED OK", MSG_OFFSET_X, MSG_OFFSET_Y, 2, false);
  tricolor_led_set_color_from_array(green);
  display_update();
  sleep(1);
}

void reedSwitchCheckFail(void) {
  display_print_message((char*)"REED BAD", MSG_OFFSET_X, MSG_OFFSET_Y, 2, false);
  tricolor_led_set_color_from_array(red);
  display_update();
  sleep(1);
}
