#!/bin/sh

error(){
    echo "Something broke. Exiting." 1>&2
    cd $HOME
    if [ -d bootstrap ]; then
        rm -rf bootstrap
    fi
    exit 1
}

echo "Setting device repo git branch: master-i2c." 1>&2
DEVICE_GIT_BRANCH="master-i2c"

echo "Pulling latest device source from git." 1>&2
cd $HOME/device || error

echo "Checking out and pulling latest device source." 1>&2
echo "Checking out and pulling $1" 1>&2
git checkout $DEVICE_GIT_BRANCH
git pull origin $DEVICE_GIT_BRANCH

echo "Setting testrig device repo git branch: master." 1>&2
TESTRIG_GIT_BRANCH="master"

echo "Pulling latest testrig device source." 1>&2
cd $HOME/testrig_device
git checkout $TESTRIG_GIT_BRANCH
git pull origin $TESTRIG_GIT_BRANCH

echo "Compiling latest testrig source." 1>&2
cd $HOME/testrig_device/c || error
make build || error

echo "Running root deploy script." 1>&2
sudo $HOME/testrig_device/scripts/deploy_as_root.sh

exit 0
