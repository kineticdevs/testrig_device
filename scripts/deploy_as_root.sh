#!/bin/sh

RUN_AS_USER=kinetic
CODE_REPO=testrig_device
BASE_DIR=/home/$RUN_AS_USER/$CODE_REPO

echo "Root: Setting up GPIO / PWM enable boot script." 1>&2

error(){
    echo "Something broke. Exiting." 1>&2
    cd $HOME
    if [ -d bootstrap ]; then
        rm -rf bootstrap
    fi
    exit 1
}

echo "  Removing old startup links..." 1>&2
/usr/sbin/update-rc.d -f export_gpio_pins.sh remove
rm -f /etc/init.d/export_gpio_pins.sh

echo "  Linking new gpio init script..." 1>&2
cp $BASE_DIR/c/scripts/export_gpio_pins.sh /etc/init.d/export_gpio_pins.sh
chmod 755 /etc/init.d/export_gpio_pins.sh
/usr/sbin/update-rc.d export_gpio_pins.sh defaults 21

if [ ! -d /home/kinetic/logs/supervisord/ ]; then
    mkdir /home/kinetic/logs/supervisord/
    chown $RUN_AS_USER /home/kinetic/logs/supervisord/
fi

echo "  Copy supervisord.conf..." 1>&2
cp $BASE_DIR/config/supervisord.conf /etc/supervisord.conf || error

echo "  Make sure supervisord starts every time." 1>&2
/usr/sbin/update-rc.d supervisord defaults || error
