
alias ts="date +%FT%H-%M-%S"

TEST_HOME="/home/kinetic/testrig_device"

mkdir -p $TEST_HOME/scripts/logs

#.. default value
TEST_MODE="all"

if [ "$1" == "functional" ]; then
	TEST_MODE="functional"
elif [ "$1" == "continuity" ]; then
	TEST_MODE="continuity"
else
	TEST_MODE="all"
fi

while :
do
    $TEST_HOME/c/board_test -v=0 -hw=2317 -test=$TEST_MODE 2>&1 | tee $TEST_HOME/scripts/logs/test.$(ts).log
done
